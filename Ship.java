import java.util.ArrayList;

public class Ship {
    public static final String SHOT_HERE = "Вы уже стреляли в это место";
    public static final String GOT = "Вы попали!";
    public static final String MISS = "Мимо";
    public static final String DESTROYED = "Корабль потоплен";
    private ArrayList coordinateShip = new ArrayList();
    private ArrayList coordinateShot = new ArrayList();

    /**
     * Метод который размешает корабль в канале
     *
     *
     */


    public Ship(int channel) {
        int a = (int) (Math.random() * (channel - 2));
        coordinateShip.add(a);
        coordinateShip.add(a + 1);
        coordinateShip.add(a + 2);

    }

    /**
     * Конструктор по умалчанию.
     * Конструктор это спецальный метод для иницыализации объекта
     */

    public Ship() {
        coordinateShip.add(0);
        coordinateShip.add(1);
        coordinateShip.add(2);
    }

    /**
     * Метод который проверят попал ли пользователь в карабль ,
     * сначала метод провиряте, стрелял ли пользователь в эту координату,
     * если да то выводится сообшенияя ("Вы уже стреляли в это место"),
     * если пользователь еще не стрялял в эту координату тогда  провиряется стоит ли там карабль,
     * если в этой координате нет коробля она добовляется в ArrayList-coordinateShot и
     * выводит сообшения ("Мимо").
     * Если пользователь ввел координату и координата совпала с координатой размешени карабля тогда из
     * ArrayList coordinateShip удоляется координата и добовляется в ArrayList coordinateShot
     * Когда ArrayList coordinateShip становится пустой то возвращает логическое значения  true.
     */


    public boolean isDestroyed(int shot) {
        if (coordinateShot.contains(shot)) {
            System.out.println(SHOT_HERE);

        } else {
            if (!coordinateShip.contains(shot)) {
                System.out.println(MISS);
                coordinateShot.add(shot);
            } else {
                int x = coordinateShip.indexOf(shot);
                coordinateShip.remove(x);
                coordinateShot.add(shot);
                System.out.println(GOT);
            }
        }
        if (coordinateShip.isEmpty()) {
            System.out.println(DESTROYED);
        }
        return coordinateShip.isEmpty();
    }

}

