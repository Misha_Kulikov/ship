import java.util.Scanner;

/**
 * Главный класс для мини игры морской бой
 */
public class Game {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        System.out.println("Введите размер канала");
        int channel = scanner.nextInt();

        while (channel < 2) {
            System.out.println("Вы ввели размер канала куда не помещается корабль, попробуйте ввести размер заного");
            channel = scanner.nextInt();
        }
        Ship ship = new Ship(channel);
        System.out.println("Вы потопили корабль за " + countAndShooting(channel, ship) + " попыток." + " Спасибо за игру");//сложения 2 строк это конкатенация!

    }

    /**
     * Метод счетающий количество выстрелов пользователя и реализующий саму стрельбу
     *
     * @return количество выстрелов
     */
    public static int countAndShooting(int channel, Ship ship) {
        boolean result;
        int count = 0;
        do {
            System.out.println("Введите координату места куда будете стрелять");
            int shot = scanner.nextInt();
            while (shot < 0 || shot >= channel) {
                System.out.println("Пробуйте ввести число которое, будет больше 0 и меньше или равно длинне канала ");
                shot = scanner.nextInt();
            }
            result = ship.isDestroyed(shot);
            count++;
        } while (!result);
        return count;
    }
}